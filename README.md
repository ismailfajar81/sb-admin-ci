# SB-Admin-2 with CRUD-CodeIgniter

Aplikasi e commerce sederhana berbasis web dengan fitur Crud yang merupakan update dari tutorial yang terdapat di situs https://www.petanikode.com/ yang sudah diupdate menggunakan **SB-Admin-2** template

## Installation


1. download CodeIgniter klik link https://codeigniter.com/download
2. lalu extract ke directory C:\xampp\htdocs untuk windows atau atau /var/www/html (di Linux)
3. Ubah nama folder sesuai project misalnya Tokobuah
4. kemudian buka http://localhost/tokobuah sehingga tampak seperti gambar
5. selamat, anda berhasil menginstall CI
6. selanjutnya lakukan kostumisasi pada Model, View dan Controller untuk menampilkan halaman yang anda inginkan
7. enjoy ...

## Screenshots

Welcome Page CodeIgniter

![Welcome Page](img/welcome page.png "Welcome page")

Login Page

![Login Page](img/login page.png "Login page")

Dashboard

![Dashboard Page](img/dashboard.png "Dashboard page")
   


